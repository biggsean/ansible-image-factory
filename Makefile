SHELL := /bin/bash
IS_CI := ${CI}
HAS_POETRY := $(shell command -v poetry)
POETRY_URL := https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py
POETRY_VERSION := 0.12.17

.PHONY: archlinux-do
archlinux-do:
	poetry run ansible-playbook \
		--inventory='localhost,' \
		ansible/archlinux-do.yml

.PHONY: bootstrap
bootstrap:
ifndef HAS_POETRY
ifdef IS_CI
	curl -sSLa \
	       $(POETRY_URL) \
	       | POETRY_VERSION=$(POETRY_VERSION) python	
else
	$(error you need to install poetry)
endif
endif
	source ${HOME}/.poetry/env && poetry install
